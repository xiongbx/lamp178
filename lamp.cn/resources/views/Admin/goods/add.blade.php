@extends('admin.layout.layout')
@section('title')
<title>LAMP178官网 - 后台商品添加</title>
<script type="text/javascript" charset="utf-8" src="{{asset('/public/utf8-php/ueditor.config.js')}}"></script>
<script type="text/javascript" charset="utf-8" src="{{asset('/public/utf8-php/ueditor.all.min.js')}}"> </script>
<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
<script type="text/javascript" charset="utf-8" src="lang/zh-cn/zh-cn.js"></script>
@endsection
@section('con')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">商品添加</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="">
    <div class="panel panel-default">
        <div class="panel-heading">
            Basic Form Elements
        </div>
        <div class="panel-body">
            <div class="row">
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form role="form" action="{{url('/admin/goods/insert')}}" method="post" enctype="multipart/form-data">
                <div class="col-lg-4 col-lg-offset-3">
                        <div class="form-group">
                            <label>商品名</label>
                            <input class="form-control" type="text" name="title" valur="{{old('title')}}">
                        </div> 
                        <div class="form-group">
                            <label>价格</label>
                            <input class="form-control" type="text" name="price" valur="{{old('price')}}">
                        </div> 
                        <div class="form-group">
                            <label>数量</label>
                            <input class="form-control" type="text" name="num" valur="{{old('num')}}">
                        </div> 
                        <div class="form-group">
                            <label>商品主图</label>
                            <input type="file" name="pic">
                        </div>
                </div>
                <div class="col-lg-10 col-lg-offset-1">
                    <script id="editor" name="content" type="text/plain" style="width:800px;height:400px;top:20px"></script>
                    
                    {{csrf_field()}}
                    <button class="btn btn-primary" type="submit">添加</button>
                    <button class="btn btn-danger" type="reset">重置</button>
                </div>
            </form>
            </div>
            <!-- /.row (nested) -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
@endsection
@section('js')
<script type="text/javascript">
    //实例化编辑器
    //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
    var ue = UE.getEditor('editor');
</script>
@endsection
