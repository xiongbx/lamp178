<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

//后台首页
Route::get('/admin/index','Admin\AdminController@index');

//后台用户管理
Route::controller('/admin/user','Admin\UserController');

//后台分类管理
Route::controller('/admin/cate','Admin\CateController');
//后台商品管理
Route::controller('/admin/goods','Admin\GoodsController');
