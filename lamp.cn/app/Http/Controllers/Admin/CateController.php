<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CateController extends Controller
{
    //分类列表
    public function getIndex(Request $request)
    {
        //查询每页显示条数
        // var_dump($request->all());
        //默认显示对少条，给了就显示给的，没给就显示默认的页
        $num = $request->input('num',10);

        //检测是否提交查询关键字
        if($request->has('keywords')){
            // echo 1;
            //分页查询 select * from user where username like '%admin%';
            $cates = DB::table('cate')->where('title','like','%'.$request->input('keywords').'%')->paginate($num);
        }else{
            // echo 2;
            $cates = DB::table('cate')->paginate($num);
        }

        //提取数据 分配到模板当中去
        $data = $request->except('page');
        // dd($cates);

        //查询所有数据
        // $users = DB::table('user')->get();
        //分页显示
        // $users = DB::table('user')->paginate($num);
        // dd($users);
        //解析模板，分配变量
        return view('Admin.cate.index',['cates'=>$cates,'data'=>$data]);
    }

    /**
     * 分类添加
     *
     * @return \Illuminate\Http\Response
     */
    public function getAdd()
    {
        // echo 222;
        //查询已有数据
        $cates = DB::table('cate')->get();

        // dd($cates);
        //解析模板
        return view('Admin.cate.add',['cates'=>$cates]);

    }

    /**
     * 分类添加处理
     *
     * @return \Illuminate\Http\Response
     */
    public function postInsert(Request $request)
    {
        // dd($request->all());
        //提取数据
        $data = $request->except('_token');

        //平判断是否为顶级分类
        if($data['pid'] == 0)
        {
            $data['path'] = '0';
            $data['status'] = '1';
        }else{
            //获取父级的path路径
             $res = DB::table('cate')->where('id',$data['pid'])->first();//就查一个用first就可以了
             //封装子类的path
            $data['path'] = $res->path.','.$data['pid'];
            // dd($res);
            $data['status'] = '1';

        }
        // dd($data); 
        //执行插入
        $res = DB::table('cate')->insert($data);
        //重定向
        if($res){
            return redirect('admin/cate/index')->with('success','用户添加成功');
        }else{
            return back()->with('error','用户添加失败');
        }
    }

    
}
