<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GoodsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * 商品添加
     *
     * @return \Illuminate\Http\Response
     */
    public function getAdd()
    {
        return view('Admin.goods.add');
    }

    /**
     * 商品添加方法
     *
     * @return \Illuminate\Http\Response
     */
    public function postInsert(Request $request)
    {
        dd($request->all());
    }
}
