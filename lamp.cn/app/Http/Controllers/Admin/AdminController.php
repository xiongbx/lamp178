<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    //后台用户列表显示
    public function index()
    {
    	// echo '后台';
        //解析后台模板
        return view('Admin.admin.index');

    }
}
