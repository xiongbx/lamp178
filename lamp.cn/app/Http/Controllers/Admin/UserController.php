<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use DB;
use Hash;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserFromPostRequest;

class UserController extends Controller
{
    /**
     * 后台用户列表
     *
     * @return 后台用户列表显示
     */
    public function getIndex(Request $request)
    {
        //查询每页显示条数
        // var_dump($request->all());
        //默认显示对少条，给了就显示给的，没给就显示默认的页
        $num = $request->input('num',10);

        //检测是否提交查询关键字
        if($request->has('keywords')){
            // echo 1;
            //分页查询 select * from user where username like '%admin%';
            $users = DB::table('user')->where('username','like','%'.$request->input('keywords').'%')->paginate($num);
        }else{
            // echo 2;
            $users = DB::table('user')->paginate($num);
        }

        //提取数据 分配到模板当中去
        $data = $request->except('page');

        //查询所有数据
        // $users = DB::table('user')->get();
        //分页显示
        // $users = DB::table('user')->paginate($num);
        // dd($users);
        //解析模板，分配变量
        return view('Admin.user.index',['users'=>$users,'data'=>$data]);
    }

    /**
     * 后台用户添加方法
     *
     * @return view用户添加表单
     *
    */
    public function getAdd()
    {
        //解析模板
        //注意：一定要注意大小写，在linux可能会有问题
        return view('Admin.user.add');
    } 

    /**
     * 后台用户处理方法
     *
     * @return view用户添加表单
     *
    */
    public function postInsert(UserFromPostRequest $request)
    {
        // 1.查看提交过来的数据
        // var_dump($request->all());
        // dd($request->all());//获取所有提交数据

        // 2.排除掉不想要的数据
        $data = $request->except(['_token']);

        //调用方法进行头像的上传操作，最后返回字符串
        $data['pic'] = $this->upload($request);

        // dd($data);

        // 4.密码加密
        $data['password'] = Hash::make($data['password']);
        // dd($data);
        
        // 3.数据入库
        $res = DB::table('user')->insertGetId($data);

        // 4.跳转到index方法中(重定向至控制器行为)
        if($res){
            return redirect('admin/user/index')->with('success','用户添加成功');
        }else{
            return back()->with('error','用户添加失败');
        }
    } 

    /**
     * 文件上传
     *
     * @return 文件上传方法
     *
    */
    public function upload($request)
    {
        //检测是否有文件上传
        if($request->hasFile('pic')){
            //随机文件名
            $name = md5(time()+rand(1,99999));
            //后缀名
            $su = $request->file('pic')->getClientOriginalExtension();
            //文件的操作是相对路径
            $request->file('pic')->move('./public/uploads',$name.'.'.$su);
            
            //返回的是绝对路径
            return '/public/uploads/'.$name.'.'.$su;
        }
    }

     /**
     * 用户删除
     *
     * @return 用户删除方法
     *
    */
    public function postDel(Request $request)
    {
        //获取数据
        $id = $request->input('id');
        
        //执行删除
        $res = DB::table('user')->where('id',$id)->delete();
        
        echo $res;
    }

    /**
     * 用户修改
     *
     * @return view用户修改
     *
    */
    public function getEdit($id)
    {
        //$id = $request->input('id');
        // dd($id);
        //根据id查询用户信息
        // $users = DB::table('user')->where('id',$id)->get();//全查询,取到多个
        $users = DB::table('user')->where('id',$id)->first();//一次拿出一条,查一个就可以了
    
        //解析模板 分配数据
        return view('Admin.user.edit',['users'=>$users]);
    }

    /**
     * 用户修改
     *
     * @return 用户方法
     *
    */
    public function postUpdate(Request $request)
    {
        $data = $request->except('_token');

        if($request->hasFile('pic')){
            //调用方法进行头像的上传操作，最后返回字符串
            $data['pic'] = $this->upload($request); 
        }
        //执行数据的修改
        
        $res = DB::table('user')->where('id',$data['id'])->update($data);

        // dd($res);

        if($res){
            //重定向
            return redirect('admin/user/index')->with('success','用户修改成功');
        }else{
            //回到上个页面
            return redirect('admin/user/index')->with('error','用户修改失败');
        }

    }
}
